from tensorflow.python.tools import inspect_checkpoint
inspect_checkpoint.print_tensors_in_checkpoint_file(
    './weights/nasnet/model.ckpt', tensor_name='', all_tensors=False, all_tensor_names=True)