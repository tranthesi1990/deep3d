from mtcnn import MTCNN
import cv2
import glob
import os

detector = MTCNN()

def get_landmark(filename):
    img = cv2.cvtColor(cv2.imread(filename), cv2.COLOR_BGR2RGB)
    lm5p_file = filename.replace('png', 'txt').replace('jpg', 'txt')
    faces = detector.detect_faces(img)
    if (len(faces) <= 0):
        os.remove(filename)
        print("remove file")
        return
    points = faces[0]['keypoints']

    # {'left_eye': (140, 207), 'right_eye': (219, 207), 'nose': (165, 254), 'mouth_left': (144, 289),
    #  'mouth_right': (226, 288)}}
    with open(lm5p_file, 'w') as file:
        file.write("{0} {1}\n".format(points['left_eye'][0], points['left_eye'][1]))
        file.write("{0} {1}\n".format(points['right_eye'][0], points['right_eye'][1]))
        file.write("{0} {1}\n".format(points['nose'][0], points['nose'][1]))
        file.write("{0} {1}\n".format(points['mouth_left'][0], points['mouth_left'][1]))
        file.write("{0} {1}\n".format(points['mouth_right'][0], points['mouth_right'][1]))

image_path = 'input'
img_list = sorted(glob.glob(image_path + '/' + '*.png'))
img_list += sorted(glob.glob(image_path + '/' + '*.jpg'))
for file in img_list:
    print(file)
    get_landmark(file)


