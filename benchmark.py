import glob
import tensorflow as tf
import numpy as np
from math import sqrt

def load_lms(lm_path):
    lm_files = sorted(glob.glob(lm_path + '/' + '*.txt'))
    lms = []
    for file in lm_files:
        lm = np.loadtxt(file, dtype=float)
        lms.append(lm)
    return lms

def cal_nme(lm_train, lm_label):
    minx, maxx = np.min(lm_label[0, :]), np.max(lm_label[0, :])
    miny, maxy = np.min(lm_label[1, :]), np.max(lm_label[1, :])
    llength = sqrt((maxx - minx) * (maxy - miny))

    dis = lm_train - lm_label
    dis = np.sqrt(np.sum(np.power(dis, 2), 1))
    dis = np.mean(dis)/llength
    return dis

lm_labels = load_lms('benchmark/lm_label')
lm_trains = load_lms('benchmark/lm_train')

nme_list = []
for i in range(len(lm_labels)):
    nme = cal_nme(lm_labels[i], lm_trains[i])
    nme_list.append(nme)

print(np.mean(nme_list))

